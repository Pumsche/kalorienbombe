﻿-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 27. Mai 2011 um 09:51
-- Server Version: 5.5.8
-- PHP-Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `kalorien`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `andrej`
--

CREATE TABLE IF NOT EXISTS `andrej` (
  `id_P` int(10) NOT NULL AUTO_INCREMENT,
  `Anzahl` varchar(150) NOT NULL,
  `Nahrungsmittel` varchar(150) NOT NULL,
  `Menge` varchar(150) DEFAULT NULL,
  `Gramm` varchar(150) NOT NULL,
  `kj` varchar(150) DEFAULT NULL,
  `kcal` varchar(150) NOT NULL,
  `Fett` varchar(150) DEFAULT NULL,
  `kh` varchar(150) NOT NULL,
  `Eiweiss` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_P`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Daten für Tabelle `andrej`
--

INSERT INTO `andrej` (`id_P`, `Anzahl`, `Nahrungsmittel`, `Menge`, `Gramm`, `kj`, `kcal`, `Fett`, `kh`, `Eiweiss`) VALUES
(1, 'Eine', 'Fischstaebchen (Oceantrader)', 'Stab', '30', '229', '55', '2', '4', '3'),
(2, 'Eine', 'Fischstaebchen (Oceantrader)', 'Stab', '30', '229', '55', '2', '4', '3'),
(3, 'Eine', 'Fischstaebchen (Oceantrader)', 'Stab', '30', '229', '55', '2', '4', '3'),
(4, 'Eine', 'Fischstaebchen (Oceantrader)', 'Stab', '30', '229', '55', '2', '4', '3'),
(5, 'Eine', 'Fischstaebchen (Oceantrader)', 'Stab', '30', '229', '55', '2', '4', '3'),
(6, 'Eine', 'Fischstaebchen (Oceantrader)', 'Stab', '30', '229', '55', '2', '4', '3'),
(7, 'Eine', 'Sonnenblumenoel (VitaDor)', 'Essloeffel', '10', '347', '83', '9', '0', '0'),
(8, 'Eine', 'Sonnenblumenoel (VitaDor)', 'Essloeffel', '10', '347', '83', '9', '0', '0'),
(9, 'Eine', 'Sonnenblumenoel (VitaDor)', 'Essloeffel', '10', '347', '83', '9', '0', '0'),
(10, 'Eine', 'Sonnenblumenoel (VitaDor)', 'Essloeffel', '10', '347', '83', '9', '0', '0'),
(11, 'Eine', 'Rapskernoel (VitaDor)', 'Essloeffel', '10', '154', '82', '9', '0', '0'),
(12, 'Eine', 'Rapskernoel (VitaDor)', 'Essloeffel', '10', '154', '82', '9', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `anja`
--

CREATE TABLE IF NOT EXISTS `anja` (
  `id_P` int(10) NOT NULL AUTO_INCREMENT,
  `Anzahl` varchar(150) NOT NULL,
  `Nahrungsmittel` varchar(150) NOT NULL,
  `Menge` varchar(150) DEFAULT NULL,
  `Gramm` varchar(150) NOT NULL,
  `kj` varchar(150) DEFAULT NULL,
  `kcal` varchar(150) NOT NULL,
  `Fett` varchar(150) DEFAULT NULL,
  `kh` varchar(150) NOT NULL,
  `Eiweiss` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_P`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `anja`
--

INSERT INTO `anja` (`id_P`, `Anzahl`, `Nahrungsmittel`, `Menge`, `Gramm`, `kj`, `kcal`, `Fett`, `kh`, `Eiweiss`) VALUES
(1, 'Eine', 'Fischstaebchen (Iglo)', 'Stab', '30', '237', '56', '2', '5', '3'),
(2, 'Eine', 'Fischstaebchen (Iglo)', 'Stab', '30', '237', '56', '2', '5', '3'),
(3, 'Eine', 'Fischstaebchen (Iglo)', 'Stab', '30', '237', '56', '2', '5', '3');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `brot`
--

CREATE TABLE IF NOT EXISTS `brot` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `Menge` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Gramm` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `Kj` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Kcal` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Fett` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Kohlenhydrate` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Eiweiß` varchar(10) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `brot`
--

INSERT INTO `brot` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Toast,Vollkorn', '1_Scheibe', '30', '307', '73', '1,4', '12,6', '2,6'),
(0, 'Sonnenblumenbrot', '1_Scheibe', '45', '475', '113', '3,6', '16,7', '3,6'),
(0, 'Weizenbrot (Frielingshof)', '1_Scheibe', '45', '471', '112', '2,8', '18', '3,8'),
(0, 'Toast (MCENNEDY)', '1_Scheibe', '36', '405', '97', '1,7', '17,6', '2,7'),
(0, 'Buttertoast', '1_Scheibe', '23', '249', '59', '0,9', '11', '1,9'),
(0, 'Baguette, Vollkorn', '1_Baguette', '50', '467', '112', '0,5', '23', '3,8'),
(0, 'Roggenmischbrot (Landgut)', '1_Scheibe', '50', '419', '100', '0,7', '20,5', '3'),
(0, 'Roggenvollkornbrot (Landgut)', '1_Scheibe', '55', '426', '102', '0,6', '20,9', '0'),
(0, 'Bagel 5 Korn', '1_Bagel', '90', '1108', '265', '5,3', '45,9', '8,5'),
(0, 'Fladenbrot', '1_Packung', '500', '4795', '1145', '5', '230', '45'),
(0, 'Weltmeisterbrot (Lidl)', '1_Scheibe', '28', '266', '64', '1,4', '11,6', '2,2');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `brot_100_gramm`
--

CREATE TABLE IF NOT EXISTS `brot_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `brot_100_gramm`
--

INSERT INTO `brot_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Toast,Vollkorn', '1', '100', '1023.33333', '243.333333', '3.33333333', '40', '6.66666666'),
(0, 'Sonnenblumenbrot', '1', '100', '1055.55555', '251.111111', '6.66666666', '35.5555555', '6.66666666'),
(0, 'Weizenbrot (Frielingshof)', '1', '100', '1046.66666', '248.888888', '4.44444444', '40', '6.66666666'),
(0, 'Toast (MCENNEDY)', '1', '100', '1125', '269.444444', '2.77777777', '47.2222222', '5.55555555'),
(0, 'Buttertoast', '1', '100', '1082.60869', '256.521739', '0', '47.8260869', '4.34782608'),
(0, 'Baguette, Vollkorn', '1', '100', '934', '224', '0', '46', '6'),
(0, 'Roggenmischbrot (Landgut)', '1', '100', '838', '200', '0', '40', '6'),
(0, 'Roggenvollkornbrot (Landgut)', '1', '100', '774.545454', '185.454545', '0', '36.3636363', '0'),
(0, 'Bagel 5 Korn', '1', '100', '1231.11111', '294.444444', '5.55555555', '50', '8.88888888'),
(0, 'Fladenbrot', '1', '100', '959', '229', '1', '46', '9'),
(0, 'Weltmeisterbrot (Lidl)', '1', '100', '950', '228.571428', '3.57142857', '39.2857142', '7.14285714');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `essen`
--

CREATE TABLE IF NOT EXISTS `essen` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `essen`
--

INSERT INTO `essen` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Putenhackfleisch', '1_Packung', '500', '3475', '830', '52', '3,5', '90'),
(0, 'Huehnerfrikassee', '1_Portion', '130', '473', '113', '5,3', '4,2', '12'),
(0, 'Putenbrust-Spiesse', '1_Spiess', '135', '458', '109', '1,4', '2,7', '21,6'),
(0, 'Putenbrustfilet, natur (Landjunker)', '1_Portion', '100', '540', '129', '3', '1,5', '24'),
(0, 'Kartoffelpueree', '1_Portion', '200', '536', '128', '1,8', '24', '4'),
(0, 'Rostbratwurst', '1_Wurst', '90', '1082', '258', '22,5', '1,4', '12,6'),
(0, 'Grillhacksteak', '1_Packung', '400', '3920', '936', '72', '14,4', '56'),
(0, 'Putenhacksteaks', '1_Steak', '100', '900', '215', '15', '3', '17'),
(0, 'Gefluegelbratwurst (Dulano)', '1_Wurst', '59', '481', '115', '8,9', '0,6', '8,3'),
(0, 'Gefluegelbratwurst (Dulano)', '1_Wurst', '59', '481', '115', '8,9', '0,6', '8,3'),
(0, 'Mini Gemuesenuggets (Dulano)', '1_Packung', '500', '4125', '985', '65', '50', '55'),
(0, 'Mini Gemuesenuggets (Dulano)', '1_Packung', '500', '4125', '985', '65', '50', '55'),
(0, 'Pizza Morzarella (Alfredo)', '1_Pizza', '350', '3280', '783', '31,2', '91', '34,3'),
(0, 'Amerikanos (Lidl)', '1_Packung', '200', '1666', '398', '14', '40', '28'),
(0, 'Backofen Pommes ', '1_Packung', '1000', '7120', '1701', '65', '250', '25'),
(0, 'Bio Kaisergemuese', '1_Packung', '750', '1005', '240', '5,3', '21,8', '17,3'),
(0, 'Bio Kaisergemuese', '1_Packung', '750', '1005', '240', '5,3', '21,8', '17,3'),
(0, 'Bratkartoffeln,Speck und Zwiebeln', '1_Packung', '500', '1695', '405', '11', '65', '9'),
(0, 'Buttergemuese (Lidl)', '1_Packung', '300', '1545', '369', '19,5', '38,5', '10,2'),
(0, 'Buttergemuese (Lidl)', '1_Packung', '300', '1545', '369', '19,5', '38,5', '10,2'),
(0, 'Haehnchenbrustfilet (Glenfell)', '1_Portion', '150', '621', '147', '2,3', '0,5', '30,3'),
(0, 'Gemuesepfanne(Green Grocer) ', '1_Packung', '750', '1890', '450', '21,75', '43,3', '20,25'),
(0, 'Gemuesepfanne(Green Grocer) ', '1_Packung', '750', '1890', '450', '21,75', '43,3', '20,25');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `essen_100_gramm`
--

CREATE TABLE IF NOT EXISTS `essen_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `essen_100_gramm`
--

INSERT INTO `essen_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Putenhackfleisch', '1', '100', '695', '166', '10.4', '0.6', '18'),
(0, 'HÃ¼hnerfrikassee', '1', '100', '363.846153', '86.9230769', '3.84615384', '3.07692307', '9.23076923'),
(0, 'Putenbrust-SpieÃŸe', '1', '100', '339.259259', '80.7407407', '0.74074074', '1.48148148', '15.5555555'),
(0, 'Putenbrustfilet, natur (Landjunker)', '1', '100', '540', '129', '3', '1', '24'),
(0, 'KartoffelpÃ¼ree', '1', '100', '268', '64', '0.5', '12', '2'),
(0, 'Rostbratwurst', '1', '100', '1202.22222', '286.666666', '24.4444444', '1.11111111', '13.3333333'),
(0, 'Grillhacksteak', '1', '100', '980', '234', '18', '3.5', '14'),
(0, 'Putenhacksteaks', '1', '100', '900', '215', '15', '3', '17'),
(0, 'GeflÃ¼gelbratwurst (Dulano)', '1', '100', '815.254237', '194.915254', '13.5593220', '0', '13.5593220'),
(0, 'Gefluegelbratwurst (Dulano)', '1', '100', '815.254237', '194.915254', '13.5593220', '0', '13.5593220'),
(0, 'Mini GemÃ¼senuggets (Dulano)', '1', '100', '825', '197', '13', '10', '11'),
(0, 'Mini Gemuesenuggets (Dulano)', '1', '100', '825', '197', '13', '10', '11'),
(0, 'Pizza Morzarella (Alfredo)', '1', '100', '937.142857', '223.714285', '8.85714285', '26', '9.71428571'),
(0, 'Amerikanos (Lidl)', '1', '100', '833', '199', '7', '20', '14'),
(0, 'Backofen Pommes ', '1', '100', '712', '170.1', '6.5', '25', '2.5'),
(0, 'Bio KaisergemÃ¼se', '1', '100', '134', '32', '0.66666666', '2.8', '2.26666666'),
(0, 'Bio Kaisergemuese', '1', '100', '134', '32', '0.66666666', '2.8', '2.26666666'),
(0, 'Bratkartoffeln,Speck und Zwiebeln', '1', '100', '339', '81', '2.2', '13', '1.8'),
(0, 'ButtergemÃ¼se (Lidl)', '1', '100', '515', '123', '6.33333333', '12.6666666', '3.33333333'),
(0, 'Buttergemuese (Lidl)', '1', '100', '515', '123', '6.33333333', '12.6666666', '3.33333333'),
(0, 'HÃ¤nchenbrustfilet (Glenfell)', '1', '100', '414', '98', '1.33333333', '0', '20'),
(0, 'GemÃ¼sepfanne(Green Grocer) ', '1', '100', '252', '60', '2.8', '5.73333333', '2.66666666'),
(0, 'Gemuesepfanne(Green Grocer) ', '1', '100', '252', '60', '2.8', '5.73333333', '2.66666666');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fette`
--

CREATE TABLE IF NOT EXISTS `fette` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `fette`
--

INSERT INTO `fette` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Laetta Halbfettmagarine', '1_Aufstrich', '20', '209,8', '74', '7,8', '0,8', '0,1'),
(0, 'Laetta Halbfettmagarine', '1_Aufstrich', '20', '209,8', '74', '7,8', '0,8', '0,1'),
(0, 'Rapskernoel (VitaDor)', '1_Essloeffel', '10', '154,9', '82,8', '9,2', '0', '0'),
(0, 'Sonnenblumenoel (VitaDor)', '1_Essloeffel', '10', '347', '83', '9,2', '0', '0'),
(0, 'Sonnenblumenoel (VitaDor)', '1_Essloeffel', '10', '347', '83', '9,2', '0', '0'),
(0, 'Laetta Halbfettmagarine', '1_Portion', '20', '209,8', '74', '7,8', '0,8', '0,1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fette_100_gramm`
--

CREATE TABLE IF NOT EXISTS `fette_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `fette_100_gramm`
--

INSERT INTO `fette_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'LÃ¤tta Halbfettmagarine', '1', '100', '1045', '370', '35', '0', '0'),
(0, 'Laetta Halbfettmagarine', '1', '100', '1045', '370', '35', '0', '0'),
(0, 'Rapskernoel (VitaDor)', '1', '100', '1540', '820', '90', '0', '0'),
(0, 'SonnenblumenÃ¶l (VitaDor)', '1', '100', '3470', '830', '90', '0', '0'),
(0, 'Sonnenblumenoel (VitaDor)', '1', '100', '3470', '830', '90', '0', '0'),
(0, 'LÃ¤tta Halbfettmagarine', '1', '100', '1045', '370', '35', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fisch`
--

CREATE TABLE IF NOT EXISTS `fisch` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `fisch`
--

INSERT INTO `fisch` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Raeucherlachs', '1_Scheibe', '20', '135,5', '32,4', '1,88', '0,02', '3,86'),
(0, 'Raeucherlachs', '1_Scheibe', '20', '135,5', '32,4', '1,88', '0,02', '3,86'),
(0, 'Fischstaebchen (Iglo)', '1_Stab', '30', '237,3', '56,7', '2,31', '5,1', '3,9'),
(0, 'Fischstaebchen (Iglo)', '1_Stab', '30', '237,3', '56,7', '2,31', '5,1', '3,9'),
(0, 'Fischstaebchen (Oceantrader)', '1_Stab', '30', '229', '55', '2,4', '4,5', '3,9'),
(0, 'Fischstaebchen (Oceantrader)', '1_Stab', '30', '229', '55', '2,4', '4,5', '3,9'),
(0, 'SchollenFilet (Nordsee)', '1_Packung', '250', '1465', '345', '4,8', '48', '33,6'),
(0, 'Garnelen,Party (LIDL)', '1_Packung', '100', '364', '87', '0,9', '0,7', '19'),
(0, 'Thunfisch in Oel (Lidl)', '1_Dose', '195', '1478', '353', '18,1', '0,2', '47,2'),
(0, 'Thunfisch eigener Saft (Nixe)', '1_Dose', '75', '353', '85', '0,4', '0', '19,2'),
(0, 'Filegro Paprika-Kraeuter (Iglo)', '1_Portion', '125', '950', '227', '13', '8,6', '19'),
(0, 'Filegro Paprika-Kraeuter (Iglo)', '1_Portion', '125', '950', '227', '13', '8,6', '19'),
(0, 'FilegroPfeffer-Paprika (Iglo)', '1_Portion', '125', '437', '104', '4,1', '2,8', '14');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fisch_100_gramm`
--

CREATE TABLE IF NOT EXISTS `fisch_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `fisch_100_gramm`
--

INSERT INTO `fisch_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'RÃ¤ucherlachs', '1', '100', '675', '160', '5', '0', '15'),
(0, 'Raeucherlachs', '1', '100', '675', '160', '5', '0', '15'),
(0, 'FischstÃ¤bchen (Iglo)', '1', '100', '790', '186.666666', '6.66666666', '16.6666666', '10'),
(0, 'Fischstaebchen (Iglo)', '1', '100', '790', '186.666666', '6.66666666', '16.6666666', '10'),
(0, 'FischstÃ¤bchen (Oceantrader)', '1', '100', '763.333333', '183.333333', '6.66666666', '13.3333333', '10'),
(0, 'Fischstaebchen (Oceantrader)', '1', '100', '763.333333', '183.333333', '6.66666666', '13.3333333', '10'),
(0, 'SchollenFilet (Nordsee)', '1', '100', '586', '138', '1.6', '19.2', '13.2'),
(0, 'Garnelen,Party (LIDL)', '1', '100', '364', '87', '0', '0', '19'),
(0, 'Thunfisch in Oel (Lidl)', '1', '100', '757.948717', '181.025641', '9.23076923', '0', '24.1025641'),
(0, 'Thunfisch eigener Saft (Nixe)', '1', '100', '470.666666', '113.333333', '0', '0', '25.3333333'),
(0, 'Filegro Paprika-KrÃ¤uter (Iglo)', '1', '100', '760', '181.6', '10.4', '6.4', '15.2'),
(0, 'Filegro Paprika-Kraeuter (Iglo)', '1', '100', '760', '181.6', '10.4', '6.4', '15.2'),
(0, 'FilegroPfeffer-Paprika (Iglo)', '1', '100', '349.6', '83.2', '3.2', '1.6', '11.2');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `getraenke`
--

CREATE TABLE IF NOT EXISTS `getraenke` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `getraenke`
--

INSERT INTO `getraenke` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Pepsi', '1_Glas', '200', '368', '88', '0', '22,2', '0'),
(0, 'Pepsi Light', '1_Glas', '200', '4', '1', '0', '0,2', '0,2'),
(0, 'Pepsi Max', '1_Glas', '200', '4', '1', '0', '0,2', '0,2'),
(0, '7up Light', '1_Glas', '200', '4', '1', '0', '0,2', '0,2'),
(0, 'Schwip Schwap Light', '1_Glas', '200', '16', '4', '0,2', '0,6', '0'),
(0, 'Mineralwasser Natur (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Medium (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Kohlensaeure (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Kohlensauure (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Black cat', '1_Glas', '200', '376', '88', '0', '20.8', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `getraenke_100_gramm`
--

CREATE TABLE IF NOT EXISTS `getraenke_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `getraenke_100_gramm`
--

INSERT INTO `getraenke_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Pepsi', '1', '100', '184', '44', '0', '11', '0'),
(0, 'Pepsi Light', '1', '100', '2', '0.5', '0', '0', '0'),
(0, 'Pepsi Max', '1', '100', '2', '0.5', '0', '0', '0'),
(0, '7up Light', '1', '100', '2', '0.5', '0', '0', '0'),
(0, 'Schwip Schwap Light', '1', '100', '8', '2', '0', '0', '0'),
(0, 'Mineralwasser Natur (Saskia)', '1', '100', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Medium (Saskia)', '1', '100', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser KohlensÃ¤ure (Saskia)', '1', '100', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Kohlensauure (Saskia)', '1', '100', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `getränke`
--

CREATE TABLE IF NOT EXISTS `getränke` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `getränke`
--

INSERT INTO `getränke` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Pepsi', '1_Glas', '200', '368', '88', '0', '22,2', '0'),
(0, 'Pepsi Light', '1_Glas', '200', '4', '1', '0', '0,2', '0,2'),
(0, 'Pepsi Max', '1_Glas', '200', '4', '1', '0', '0,2', '0,2'),
(0, '7up Light', '1_Glas', '200', '4', '1', '0', '0,2', '0,2'),
(0, 'Schwip Schwap Light', '1_Glas', '200', '16', '4', '0,2', '0,6', '0'),
(0, 'Mineralwasser Natur (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Medium (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser KohlensÃ¤ure (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Kohlensauure (Saskia)', '1_Glas', '200', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `getränke_100_gramm`
--

CREATE TABLE IF NOT EXISTS `getränke_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `getränke_100_gramm`
--

INSERT INTO `getränke_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Pepsi', '1', '100', '184', '44', '0', '11', '0'),
(0, 'Pepsi Light', '1', '100', '2', '0.5', '0', '0', '0'),
(0, 'Pepsi Max', '1', '100', '2', '0.5', '0', '0', '0'),
(0, '7up Light', '1', '100', '2', '0.5', '0', '0', '0'),
(0, 'Schwip Schwap Light', '1', '100', '8', '2', '0', '0', '0'),
(0, 'Mineralwasser Natur (Saskia)', '1', '100', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Medium (Saskia)', '1', '100', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser KohlensÃ¤ure (Saskia)', '1', '100', '0', '0', '0', '0', '0'),
(0, 'Mineralwasser Kohlensauure (Saskia)', '1', '100', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `joerg`
--

CREATE TABLE IF NOT EXISTS `joerg` (
  `id_P` int(10) NOT NULL AUTO_INCREMENT,
  `Anzahl` varchar(150) NOT NULL,
  `Nahrungsmittel` varchar(150) NOT NULL,
  `Menge` varchar(150) DEFAULT NULL,
  `Gramm` varchar(150) NOT NULL,
  `kj` varchar(150) DEFAULT NULL,
  `kcal` varchar(150) NOT NULL,
  `Fett` varchar(150) DEFAULT NULL,
  `kh` varchar(150) NOT NULL,
  `Eiweiss` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_P`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `joerg`
--

INSERT INTO `joerg` (`id_P`, `Anzahl`, `Nahrungsmittel`, `Menge`, `Gramm`, `kj`, `kcal`, `Fett`, `kh`, `Eiweiss`) VALUES
(1, 'Eine', 'Gefluegel Mortadella (Dulano)', 'Scheibe', '15', '186', '44', '1', '0', '1'),
(2, 'Eine', 'Gefluegel Mortadella (Dulano)', 'Scheibe', '15', '186', '44', '1', '0', '1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kaese`
--

CREATE TABLE IF NOT EXISTS `kaese` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `kaese`
--

INSERT INTO `kaese` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Gouda light 16% (Linessa)', '1_Scheibe', '40', '429', '102', '6,4', '0,01', '11,2'),
(0, 'Massdamer', '1_Scheibe', '38', '557', '132', '10,3', '0,001', '9,9'),
(0, 'Gouda', '1_Scheibe', '40', '593', '142', '11,6', '0,001', '9,6'),
(0, 'Kaeseaufschnitt Light (Linessa)', '1_Scheibe', '35', '370', '88', '5,6', '0,05', '9,5'),
(0, 'Butterkaese 45%', '1_Scheibe', '40', '568', '136', '10,8', '0,001', '9,6'),
(0, 'Raeucherkaese Pur', '1_Scheibe', '23', '301', '72', '5,8', '0,2', '4,8'),
(0, 'Raeucherkaese Schinken', '1_Scheibe', '18', '236', '56', '4,5', '0,2', '3,8'),
(0, 'Kaese Raeucherschinken (Groenland)', '1_Scheibe', '20', '281', '67', '5,1', '0,1', '3,9'),
(0, 'Kaesescheibletten', 'Scheibe', '25', '209', '50', '3', '1,5', '4,3');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kaese_100_gramm`
--

CREATE TABLE IF NOT EXISTS `kaese_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `kaese_100_gramm`
--

INSERT INTO `kaese_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Gouda light 16% (Linessa)', '1', '100', '1072.5', '255', '15', '0', '27.5'),
(0, 'Massdamer', '1', '100', '1465.78947', '347.368421', '26.3157894', '0', '23.6842105'),
(0, 'Gouda', '1', '100', '1482.5', '355', '27.5', '0', '22.5'),
(0, 'KÃ¤seaufschnitt Light (Linessa)', '1', '100', '1057.14285', '251.428571', '14.2857142', '0', '25.7142857'),
(0, 'ButterkÃ¤se 45%', '1', '100', '1420', '340', '25', '0', '22.5'),
(0, 'RÃ¤ucherkÃ¤se Pur', '1', '100', '1308.69565', '313.043478', '21.7391304', '0', '17.3913043'),
(0, 'RÃ¤ucherkÃ¤se Schinken', '1', '100', '1311.11111', '311.111111', '22.2222222', '0', '16.6666666'),
(0, 'KÃ¤se RÃ¤ucherschinken (GrÃ¼nland)', '1', '100', '1405', '335', '25', '0', '15');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `käse`
--

CREATE TABLE IF NOT EXISTS `käse` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `käse`
--

INSERT INTO `käse` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Gouda light 16% (Linessa)', '1_Scheibe', '40', '429', '102', '6,4', '0,01', '11,2'),
(0, 'Massdamer', '1_Scheibe', '38', '557', '132', '10,3', '0,001', '9,9'),
(0, 'Gouda', '1_Scheibe', '40', '593', '142', '11,6', '0,001', '9,6'),
(0, 'KÃ¤seaufschnitt Light (Linessa)', '1_Scheibe', '35', '370', '88', '5,6', '0,05', '9,5'),
(0, 'ButterkÃ¤se 45%', '1_Scheibe', '40', '568', '136', '10,8', '0,001', '9,6'),
(0, 'RÃ¤ucherkÃ¤se Pur', '1_Scheibe', '23', '301', '72', '5,8', '0,2', '4,8'),
(0, 'RÃ¤ucherkÃ¤se Schinken', '1_Scheibe', '18', '236', '56', '4,5', '0,2', '3,8'),
(0, 'KÃ¤se RÃ¤ucherschinken (GrÃ¼nland)', '1_Scheibe', '20', '281', '67', '5,1', '0,1', '3,9');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `käse_100_gramm`
--

CREATE TABLE IF NOT EXISTS `käse_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `käse_100_gramm`
--

INSERT INTO `käse_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Gouda light 16% (Linessa)', '1', '100', '1072.5', '255', '15', '0', '27.5'),
(0, 'Massdamer', '1', '100', '1465.78947', '347.368421', '26.3157894', '0', '23.6842105'),
(0, 'Gouda', '1', '100', '1482.5', '355', '27.5', '0', '22.5'),
(0, 'KÃ¤seaufschnitt Light (Linessa)', '1', '100', '1057.14285', '251.428571', '14.2857142', '0', '25.7142857'),
(0, 'ButterkÃ¤se 45%', '1', '100', '1420', '340', '25', '0', '22.5'),
(0, 'RÃ¤ucherkÃ¤se Pur', '1', '100', '1308.69565', '313.043478', '21.7391304', '0', '17.3913043'),
(0, 'RÃ¤ucherkÃ¤se Schinken', '1', '100', '1311.11111', '311.111111', '22.2222222', '0', '16.6666666'),
(0, 'KÃ¤se RÃ¤ucherschinken (GrÃ¼nland)', '1', '100', '1405', '335', '25', '0', '15');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `milch_joghurt`
--

CREATE TABLE IF NOT EXISTS `milch_joghurt` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `milch_joghurt`
--

INSERT INTO `milch_joghurt` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Actimel, Erdbeere', '1_Becher', '100', '316', '75', '1,5', '11,8', '2,7'),
(0, 'Actimel, Vanille', '1_Becher', '100', '337', '80', '1,5', '13', '2,7'),
(0, 'Actimel, Kirsche', '1_Becher', '100', '310', '74', '1,5', '11,5', '2,9'),
(0, 'Ei (Mittelgross)', '1_Stueck', '63', '409', '98', '7,1', '0,4', '8,3'),
(0, 'Ei (Mittelgross)', '1_Stueck', '63', '409', '98', '7,1', '0,4', '8,3');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `milch_joghurt_100_gramm`
--

CREATE TABLE IF NOT EXISTS `milch_joghurt_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `milch_joghurt_100_gramm`
--

INSERT INTO `milch_joghurt_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Actimel, Erdbeere', '1', '100', '316', '75', '1', '11', '2'),
(0, 'Actimel, Vanille', '1', '100', '337', '80', '1', '13', '2'),
(0, 'Actimel, Kirsche', '1', '100', '310', '74', '1', '11', '2'),
(0, 'Ei (MittelgroÃŸ)', '1', '100', '649.206349', '155.555555', '11.1111111', '0', '12.6984126'),
(0, 'Ei (Mittelgross)', '1', '100', '649.206349', '155.555555', '11.1111111', '0', '12.6984126');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `naschen`
--

CREATE TABLE IF NOT EXISTS `naschen` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `naschen`
--

INSERT INTO `naschen` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Gelatelli Schoko Vanille Eis', '1_Portion', '75', '926', '221', '10,6', '28,2', '2,7'),
(0, 'Gelatelli Trio Choc Eis', '1_Portion', '120ml', '1442', '344', '16,8', '42,6', '4,4'),
(0, 'Crema die Nocciolato, Nuss Eis', '1_Portion', '100ml', '867', '207', '10,5', '24,7', '3,4');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `naschen_100_gramm`
--

CREATE TABLE IF NOT EXISTS `naschen_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `naschen_100_gramm`
--


-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pupi`
--

CREATE TABLE IF NOT EXISTS `pupi` (
  `id_P` int(10) NOT NULL AUTO_INCREMENT,
  `Anzahl` varchar(150) NOT NULL,
  `Nahrungsmittel` varchar(150) NOT NULL,
  `Menge` varchar(150) DEFAULT NULL,
  `Gramm` varchar(150) NOT NULL,
  `kj` varchar(150) DEFAULT NULL,
  `kcal` varchar(150) NOT NULL,
  `Fett` varchar(150) DEFAULT NULL,
  `kh` varchar(150) NOT NULL,
  `Eiweiss` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_P`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Daten für Tabelle `pupi`
--

INSERT INTO `pupi` (`id_P`, `Anzahl`, `Nahrungsmittel`, `Menge`, `Gramm`, `kj`, `kcal`, `Fett`, `kh`, `Eiweiss`) VALUES
(1, 'Eine', 'Light Salami 1A', 'Scheibe', '3', '34', '8', '0', '0', '0'),
(2, 'Eine', 'Light Salami 1A', 'Scheibe', '3', '34', '8', '0', '0', '0'),
(3, 'Eine', 'Light Salami 1A', 'Scheibe', '3', '34', '8', '0', '0', '0'),
(4, 'Eine', 'Sonnenblumenoel (VitaDor)', 'Essloeffel', '10', '347', '83', '9', '0', '0'),
(5, 'Eine', 'Sonnenblumenoel (VitaDor)', 'Essloeffel', '10', '347', '83', '9', '0', '0'),
(6, 'Eine', 'Light Salami 1A Paprika', 'Scheibe', '4', '45', '11', '0', '0', '0'),
(7, 'Eine', 'Light Salami 1A Paprika', 'Scheibe', '4', '45', '11', '0', '0', '0'),
(8, 'Eine', 'Rapskernoel (VitaDor)', 'Essloeffel', '10', '154', '82', '9', '0', '0'),
(9, 'Eine', 'Rapskernoel (VitaDor)', 'Essloeffel', '10', '154', '82', '9', '0', '0'),
(10, 'Dreifache', 'Pepsi', 'Glas', '600', '1104', '264', '0', '66', '0'),
(11, 'Dreifache', 'Pepsi', 'Glas', '600', '1104', '264', '0', '66', '0'),
(12, 'Dreifache', 'Pepsi', 'Glas', '600', '1104', '264', '0', '66', '0'),
(13, 'Dreifache', 'Pepsi', 'Glas', '600', '1104', '264', '0', '66', '0'),
(14, 'Eine', 'Kaeseaufschnitt Light (Linessa)', 'Scheibe', '35', '370', '88', '5', '0', '9'),
(15, 'Eine', 'Kaeseaufschnitt Light (Linessa)', 'Scheibe', '35', '370', '88', '5', '0', '9');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `salat`
--

CREATE TABLE IF NOT EXISTS `salat` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `salat`
--

INSERT INTO `salat` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Wassermelone', '1_Portion', '100g', '159', '37', '0,2', '8,3', '0,6');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `salat_100_gramm`
--

CREATE TABLE IF NOT EXISTS `salat_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `salat_100_gramm`
--


-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wurst`
--

CREATE TABLE IF NOT EXISTS `wurst` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `wurst`
--

INSERT INTO `wurst` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Delikatess Lachsschinken (Dulano)', '1_Scheibe', '100', '465', '111', '2', '1', '22'),
(0, 'Gefluegel Mortadella (Dulano)', '1_Scheibe', '15', '186', '44', '1,7', '0,2', '1,7'),
(0, 'Hinterkochschinken (Gebirgsjaeger)', '1_Scheibe', '35', '158', '38', '1,1', '0,4', '6,7'),
(0, 'Streichzwerge Leberwurst Light', '1_Aufstrich', '24', '247', '59', '4,8', '0,4', '3,6'),
(0, 'Light Salami 1A', '1_Scheibe', '3', '34', '8', '0,6', '0', '0,6'),
(0, 'Paprika Salami (Gebirgsjaeger)', '1_Scheibe', '15', '226', '54', '4,8', '0,2', '2,6'),
(0, 'Light Salami 1A Paprika', '1_Scheibe', '4', '45', '11', '0,8', '0', '0,8'),
(0, 'Putensalami, hauchduenn (Lidl)', '1_Scheibe', '6', '68', '16', '1,3', '0,1', '1,2'),
(0, 'Bacon Gewuerfelt (Dulano)', '1_Portion', '50', '316', '75', '6,3', '0,2', '4,6'),
(0, 'Mortadella (Metzgerfrisch)', '1_Scheibe', '14', '135', '32', '2,8', '0,1', '1,7'),
(0, 'Haehnchenbrust (Dulano)', '1_Scheibe', '20', '81', '19', '0,2', '0,2', '4,2'),
(0, 'Truthahnsalami (Gebirgsjaeger)', '1_Scheibe', '20', '271', '65', '5,6', '0,2', '3,4'),
(0, 'Salami 1A (Gebirgsjaeger)', '1_Scheibe', '7', '102', '24', '2,1', '0,1', '1,3');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wurst_100_gramm`
--

CREATE TABLE IF NOT EXISTS `wurst_100_gramm` (
  `ID_F` int(11) NOT NULL,
  `Nachrungsmittel` varchar(150) DEFAULT NULL,
  `Menge` varchar(50) DEFAULT NULL,
  `Gramm` varchar(15) DEFAULT NULL,
  `Kj` varchar(10) DEFAULT NULL,
  `Kcal` varchar(10) DEFAULT NULL,
  `Fett` varchar(10) DEFAULT NULL,
  `Kohlenhydrate` varchar(10) DEFAULT NULL,
  `Eiweiß` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `wurst_100_gramm`
--

INSERT INTO `wurst_100_gramm` (`ID_F`, `Nachrungsmittel`, `Menge`, `Gramm`, `Kj`, `Kcal`, `Fett`, `Kohlenhydrate`, `Eiweiß`) VALUES
(0, 'Delikatess Lachsschinken (Dulano)', '1', '100', '465', '111', '2', '1', '22'),
(0, 'GeflÃ¼gel Mortadella (Dulano)', '1', '100', '1240', '293.333333', '6.66666666', '0', '6.66666666'),
(0, 'Hinterkochschinken (GebirgsjÃ¤ger)', '1', '100', '451.428571', '108.571428', '2.85714285', '0', '17.1428571'),
(0, 'Streichzwerge Leberwurst Light', '1', '100', '1029.16666', '245.833333', '16.6666666', '0', '12.5'),
(0, 'Light Salami 1A', '1', '100', '1133.33333', '266.666666', '0', '0', '0'),
(0, 'Paprika Salami (GebirgsjÃ¤ger)', '1', '100', '1506.66666', '360', '26.6666666', '0', '13.3333333'),
(0, 'Light Salami 1A Paprika', '1', '100', '1125', '275', '0', '0', '0'),
(0, 'Putensalami, hauchdÃ¼nn (Lidl)', '1', '100', '1133.33333', '266.666666', '16.6666666', '0', '16.6666666'),
(0, 'Bacon GewÃ¼rfelt (Dulano)', '1', '100', '632', '150', '12', '0', '8'),
(0, 'Mortadella (Metzgerfrisch)', '1', '100', '964.285714', '228.571428', '14.2857142', '0', '7.14285714'),
(0, 'HÃ¤nchenbrust (Dulano)', '1', '100', '405', '95', '0', '0', '20'),
(0, 'Truthahnsalami (GebirgsjÃ¤ger)', '1', '100', '1355', '325', '25', '0', '15'),
(0, 'Salami 1A (GebirgsjÃ¤ger)', '1', '100', '1457.14285', '342.857142', '28.5714285', '0', '14.2857142');
