<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
	<title>Demo Page: Using Progressive Enhancement to Convert a Select Box Into an Accessible jQuery UI Slider</title>
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.7.1.custom.min.js"></script>
	<script type="text/javascript" src="js/selectToUISlider.jQuery.js"></script>		
	 <link rel="stylesheet" href="css/redmond/jquery-ui-1.7.1.custom.css" type="text/css" />
	<link rel="Stylesheet" href="css/ui.slider.extras.css" type="text/css" />  
	
	<style type="text/css">
		body {font-size: 62.5%; font-family:"Segoe UI","Helvetica Neue",Helvetica,Arial,sans-serif; }
		fieldset { border:0; margin: 6em; height: 12em;}	
		label {font-weight: normal; float: left; margin-right: .5em; font-size: 1.1em;}
		select {margin-right: 1em; float: left;}
		.ui-slider {clear: both; top: 5em;}
	</style>
	
	<script type="text/javascript">	
	</script>
	<!-- jQuery UI theme switcher -->
	<script type="text/javascript" src="http://ui.jquery.com/applications/themeroller/themeswitchertool/"></script>	
</head>

<body>
		<script type="text/javascript" src="js/slider3.js"></script>
		<script type="text/javascript"> $(function(){ $('<div style=" right: 20px; margin-top: -40px" />').appendTo('body').themeswitcher({onSelect: function(){ setTimeout(fixToolTipColor, 800); }}); });
		</script>
	<form action='test.php' method="post">
		<!-- demo 1 -->
		<fieldset>
			<label for="speed"></label>
			<select name="speed" id="speed" style="display:none" >
				<option value="0,25">Viertel Portion</option>
				<option value="0,5">Halbe Portion</option>
				<option value="0,75">Dreiviertel Portion</option>
				<option value="1"  selected="selected">Ganze Portion</option>
				<option value="1,5">Eineinhalb Portionen</option>
				<option value="2">Doppelte Portion</option>
				<option value="4">Vier Portionen</option>
				<option value="8">Acht Portionen</option>
			</select>
			<input type="submit" value=" Absenden " name="senden" />			
		</fieldset>				
		</form>
		

</body>

</html>
