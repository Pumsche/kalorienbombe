﻿-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 27. Mai 2011 um 09:50
-- Server Version: 5.5.8
-- PHP-Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `kalori_767419`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kcalkillersport`
--

CREATE TABLE IF NOT EXISTS `kcalkillersport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sportart` varchar(30) DEFAULT NULL,
  `Faktor` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Daten für Tabelle `kcalkillersport`
--

INSERT INTO `kcalkillersport` (`ID`, `Sportart`, `Faktor`) VALUES
(1, 'Aerobic', '10,6'),
(2, 'Badminton', '10'),
(3, 'Basketball', '12,5'),
(4, 'Billard', '4,3'),
(5, 'Gerätetraining', '18,4'),
(6, 'Fussball', '13,2'),
(7, 'Gymnastik', '9,3'),
(8, 'Handball', '14,4'),
(9, 'Inline-Skating', '12'),
(11, 'Joggen (1 km in 7 min)', '13,6'),
(12, 'Joggen (1 km in 5 min)', '20,8'),
(13, 'Joggen (1 km in 3,5 min)', '28,9'),
(14, 'Mountainbiking', '14,4'),
(15, ' Rad fahren 15 km/h', '10'),
(16, ' Rad fahren 25 km/h', '17'),
(17, ' Reiten', '13,7'),
(18, ' Rudern', '14,4'),
(19, ' Schwimmen (langsam)', '12,8'),
(20, ' Schwimmen (schnell)', '15,6'),
(21, ' Squash', '20,2'),
(22, ' Tanzen', '6,8'),
(23, ' Tanzen(klassisch)', '12,2'),
(24, ' Tennis', '11'),
(25, ' Tischtennis', '6,9'),
(26, ' Volleyball', '5,8'),
(27, ' Walking', '11');
